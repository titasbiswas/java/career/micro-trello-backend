# Prerequisites to run the app
`Java 17`
`Docker`
## Run local db through docker 
`docker run --name trl-postgres -e POSTGRES_PASSWORD=trl_secret -e POSTGRES_USER=trl_user -e POSTGRES_DB=trl_db -p 5432:5432 -d postgres:14.5-alpine`

## Start the application 
This is a simple spring boot application, it can be run with 
`./gradlew bootRun`

OR

Running the `src/main/java/titas/micro/trello/MicroTrelloBackendApplication.java` class from any ide


## Endpoints are  listed at 
http://localhost:8080/swagger-ui/index.html#/

As spring security has not been implemented and a user is required, the functionality has been mocked with a 
userName paramter with every endpoint (except create user), the default `userName` that is created while starting the application is `titas`

A sample board for testing the endpoints can be crerated with the endpoint
http://localhost:8080/swagger-ui/index.html#/board-controller/createBoard and sample json data from:
`src/test/resources/data/SampleBoard.json`