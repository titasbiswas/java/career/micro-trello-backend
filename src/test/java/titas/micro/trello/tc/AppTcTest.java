package titas.micro.trello.tc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import titas.micro.trello.entities.Board;
import titas.micro.trello.entities.BoardColumn;
import titas.micro.trello.entities.Card;
import titas.micro.trello.repositories.BoardColumnRepository;
import titas.micro.trello.repositories.UserRepository;
import titas.micro.trello.services.BoardService;
import titas.micro.trello.services.CardServices;
import titas.micro.trello.services.UserService;
import titas.micro.trello.utils.HelperData;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("tc")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppTcTest {

    @Autowired
    private BoardService boardService;

    @Autowired
    private CardServices cardServices;

    @Autowired
    private UserService userService;

    @Autowired
    private HelperData helperData;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardColumnRepository boardColumnRepository;


    @BeforeAll
    void initTestEnv() throws IOException {

        helperData.setCurrentUser(userRepository
                .findByUserName("titas").get());

        File resource = new ClassPathResource(
                "data/SampleBoard.json").getFile();
        String boardJson = new String(Files.readAllBytes(resource.toPath()));
        Board board = new ObjectMapper().readValue(boardJson, Board.class);
        boardService.createBoard(board);
    }


    @Test
    void getBoardByName(){

        Optional<Board> boardOptional = boardService.getBoardByName("Team Phoenix");
        assertNotNull(boardOptional.get());
    }

    @Test
    @Transactional
    void getCardsByColumnId() {
        Optional<BoardColumn> boardColumnOptional = boardColumnRepository.findByName("Backlog");
        List<Card> cards = cardServices.getCardsByColumnId(boardColumnOptional.get().getId());
        assertEquals(2, cards.size());
    }

    @Test
    void getCardsByLabel() {
        List<Card> cards = cardServices.getCardsByLabel("P1");
        assertEquals(3, cards.size());
    }

    @Test
    void getCardsCreatedAfterDate() {
        ZonedDateTime oneMinBack = ZonedDateTime.now().minus(1, ChronoUnit.MINUTES);
        List<Card> cards = cardServices.getCardsCreatedAfterDate(Date.from(oneMinBack.toInstant()));
        assertEquals(7, cards.size());
    }

    @Test
    @Transactional
    void getCardsByUserAndFromDate() throws InterruptedException {


        TimeUnit.SECONDS.sleep(3);
        List<Card> cards = cardServices.getCardsByLabel("CUST_ISSUE");
        Card card = cards.get(0);
        card.setDescription("Some very new description for this cust issue");
        Card updatedCard = cardServices.updateCard(card.getId(), card);
        ZonedDateTime twoSecBack = ZonedDateTime.now().minus(2, ChronoUnit.SECONDS);
        userService.updateLastLogInTime(Date.from(twoSecBack.toInstant()));
        List<Card> highlitedCardsForUser = cardServices.getHighlitedCardsForUser(helperData.getCurrentUser().getUserName());
        assertEquals(1, highlitedCardsForUser.size());
    }
}