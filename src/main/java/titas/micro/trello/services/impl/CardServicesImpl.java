package titas.micro.trello.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import titas.micro.trello.entities.BoardColumn;
import titas.micro.trello.entities.Card;
import titas.micro.trello.entities.User;
import titas.micro.trello.repositories.BoardColumnRepository;
import titas.micro.trello.repositories.CardRepository;
import titas.micro.trello.services.CardServices;
import titas.micro.trello.services.UserService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CardServicesImpl implements CardServices {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private BoardColumnRepository boardColumnRepository;

    @Autowired
    private UserService userService;



    @Override
    public Card createCard(Card card) {
        return cardRepository.save(card);
    }

    @Override
    public Card updateCard(Long cardId, Card card) {
        cardRepository.findById(cardId)
                .orElseThrow();
        card.setId(cardId);
        return cardRepository.save(card);
    }

    @Override
    public void deleteCard(Long cardId) {
        cardRepository.findById(cardId)
                .orElseThrow();
        cardRepository.deleteById(cardId);

    }

    @Override
    public List<Card> getCardsByColumnId(Long columnId) {
        BoardColumn boardColumn =  boardColumnRepository.findById(columnId).orElseThrow();
        return boardColumn.getCards().stream().toList();
    }

    @Override
    public List<Card> getCardsByLabel(String label) {
        return cardRepository.findByLabels_LabelName(label);
    }

    @Override
    public List<Card> getCardsCreatedAfterDate(Date fromDate) {
        return cardRepository.findByCreationDateGreaterThanEqual(fromDate);
    }

    @Override
    public List<Card> getHighlitedCardsForUser(String userName) {
        Optional<User> optionalUser = userService.getByUserName(userName);
        Date latLoggedInTime = optionalUser.orElseThrow().getLastLoggedIn();
        List<Card> cards =  cardRepository
                .findByCreationDateGreaterThanEqualOrLastModifiedDateGreaterThanEqual(latLoggedInTime, latLoggedInTime);
        userService.updateLastLogInTime(new Date());
        return cards;
    }
}
