package titas.micro.trello.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import titas.micro.trello.entities.Board;
import titas.micro.trello.repositories.BoardRepository;
import titas.micro.trello.services.BoardService;
import titas.micro.trello.services.UserService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BoardServiceImpl implements BoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private UserService userService;

    @Override
    public Optional<Board> getBoardById(Long boardId) {
        Optional<Board> boardOptional=  boardRepository.findById(boardId);
        userService.updateLastLogInTime(new Date());
        return boardOptional;
    }

    @Override
    public Optional<Board> getBoardByName(String name) {
        Optional<Board> boardOptional = boardRepository.findByName(name);
        userService.updateLastLogInTime(new Date());
        return boardOptional;
    }

    @Override
    public List<Board> getAllBoards() {
        return boardRepository.findAll();
    }

    @Override
    public Board createBoard(Board board) {
        return boardRepository.save(board);
    }
}
