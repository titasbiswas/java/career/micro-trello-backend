package titas.micro.trello.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import titas.micro.trello.entities.User;
import titas.micro.trello.repositories.UserRepository;
import titas.micro.trello.services.UserService;
import titas.micro.trello.utils.HelperData;

import java.util.Date;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HelperData helperData;

    @Override
    public void updateLastLogInTime(Date date) {
        helperData.getCurrentUser().setLastLoggedIn(date);
        userRepository.save(helperData.getCurrentUser());

    }

    @Override
    public User createNewUser(User user) {
       return userRepository.save(user);
    }

    @Override
    public Optional<User> getByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
}
