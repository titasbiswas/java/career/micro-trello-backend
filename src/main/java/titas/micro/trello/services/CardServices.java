package titas.micro.trello.services;

import titas.micro.trello.entities.Card;

import java.util.Date;
import java.util.List;

public interface CardServices {
    Card createCard(Card card);
    Card updateCard(Long cardId, Card card);
    void deleteCard(Long cardId);
    List<Card> getCardsByColumnId(Long columnId);

    List<Card> getCardsByLabel(String label);

    List<Card> getCardsCreatedAfterDate(Date fromDate);
    List<Card> getHighlitedCardsForUser(String userName);

}
