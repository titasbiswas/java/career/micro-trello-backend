package titas.micro.trello.services;

import titas.micro.trello.entities.Board;

import java.util.List;
import java.util.Optional;

public interface BoardService {

    Optional<Board> getBoardById(Long boardId);
    Optional<Board> getBoardByName(String name);
    List<Board> getAllBoards();
    Board createBoard(Board board);
}
