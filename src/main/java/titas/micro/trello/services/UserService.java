package titas.micro.trello.services;

import titas.micro.trello.entities.User;

import java.util.Date;
import java.util.Optional;

public interface UserService {
    void updateLastLogInTime(Date date);
    User createNewUser(User user);
    Optional<User> getByUserName(String userName);

}
