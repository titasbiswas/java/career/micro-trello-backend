package titas.micro.trello.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import titas.micro.trello.entities.User;
import titas.micro.trello.services.UserService;

import java.util.Date;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

   @PostMapping("/last-login")
    ResponseEntity<Void> updateLatLoginTime(@RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
                                            @RequestParam(name = "userName") String userName){
       userService.updateLastLogInTime(date);
       return ResponseEntity.noContent().build();
   }

   @PostMapping
    ResponseEntity<User> createUser(@RequestBody User user){
       return new ResponseEntity<>(userService.createNewUser(user), HttpStatus.CREATED);
   }

}
