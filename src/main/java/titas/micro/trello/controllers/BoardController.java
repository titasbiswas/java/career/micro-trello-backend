package titas.micro.trello.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import titas.micro.trello.entities.Board;
import titas.micro.trello.services.BoardService;

import java.util.List;

@RestController
@RequestMapping("/board")
public class BoardController {

    @Autowired
    private BoardService boardService;

    @GetMapping()
    ResponseEntity<List<Board>> getBoards(@RequestParam(name = "userName") String userName){
        return ResponseEntity.ok(boardService.getAllBoards());
    }

    @PostMapping
    ResponseEntity<Board> createBoard(@RequestBody Board board, @RequestParam(name = "userName") String userName){
        return new ResponseEntity<>(boardService.createBoard(board), HttpStatus.CREATED);
    }
}
