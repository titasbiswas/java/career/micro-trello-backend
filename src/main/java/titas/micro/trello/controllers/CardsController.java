package titas.micro.trello.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import titas.micro.trello.entities.Card;
import titas.micro.trello.services.CardServices;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/cards")
public class CardsController {

    @Autowired
    private CardServices cardServices;

    @GetMapping("/tag")
    ResponseEntity<List<Card>> getCardsByLabel(@RequestParam(name = "label") String label,
                                               @RequestParam(name = "userName") String userName) {
        return ResponseEntity.ok(cardServices.getCardsByLabel(label));
    }

    @GetMapping("/column")
    ResponseEntity<List<Card>> getCardsByColumnId(@RequestParam(name = "columnId") Long columnId,
                                                  @RequestParam(name = "userName") String userName) {
        return ResponseEntity.ok(cardServices.getCardsByColumnId(columnId));
    }

    @GetMapping("/after")
    ResponseEntity<List<Card>> getCardsCreatedAfter(@RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fromDate,
                                                    @RequestParam(name = "userName") String userName) {
        return ResponseEntity.ok(cardServices.getCardsCreatedAfterDate(fromDate));
    }

    @GetMapping("/highlight")
    ResponseEntity<List<Card>> getCardsAccessedAfterByUser(
            @RequestParam(name = "userName") String userName) {
        return ResponseEntity.ok(cardServices.getHighlitedCardsForUser(userName));
    }

    @PostMapping
    ResponseEntity<Card> createCard(@RequestBody Card card,
                                    @RequestParam(name = "userName") String userName) {
        return new ResponseEntity<>(cardServices.createCard(card), HttpStatus.CREATED);
    }

    @PutMapping
    ResponseEntity<Card> updateCard(@RequestBody Card card,
                                    @RequestParam(name = "id") Long id,
                                    @RequestParam(name = "userName") String userName) {
        return new ResponseEntity<>(cardServices.updateCard(id, card), HttpStatus.OK);
    }

    @DeleteMapping
    ResponseEntity<Void> deleteCard(
            @RequestParam(name = "id") Long id,
            @RequestParam(name = "userName") String userName) {
        cardServices.deleteCard(id);
        return ResponseEntity.noContent().build();
    }
}
