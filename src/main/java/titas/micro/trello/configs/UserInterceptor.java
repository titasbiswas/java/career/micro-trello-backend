package titas.micro.trello.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import titas.micro.trello.entities.User;
import titas.micro.trello.repositories.UserRepository;
import titas.micro.trello.utils.HelperData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserInterceptor implements HandlerInterceptor {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HelperData helperData;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String userName = request.getParameter("userName");
        helperData.setCurrentUser(userRepository
                .findByUserName(userName != null ? userName.trim() : "")
                .orElse(new User()));
        return true;
    }

}
