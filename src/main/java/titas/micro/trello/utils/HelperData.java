package titas.micro.trello.utils;

import lombok.Data;
import org.springframework.stereotype.Component;
import titas.micro.trello.entities.User;

@Component
@Data
public class HelperData {
    private User currentUser;
}
