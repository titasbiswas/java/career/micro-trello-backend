package titas.micro.trello.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import titas.micro.trello.entities.User;

import java.util.Optional;

@Component
public class AuditorAwareImpl implements AuditorAware<User> {

    @Autowired
    private HelperData helperData;


    @Override
    public Optional<User> getCurrentAuditor() {
        return Optional.ofNullable(helperData.getCurrentUser());

    }
}
