package titas.micro.trello.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import titas.micro.trello.entities.User;
import titas.micro.trello.repositories.BoardRepository;
import titas.micro.trello.repositories.UserRepository;

import java.util.Date;

@Component
@Profile({"local", "tc"})
public class InitLocal implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BoardRepository boardRepository;

    @Override
    public void run(String...args) throws Exception {

        User initialUser =  User.builder()
                .userName("titas")
                .firstName("Titas")
                .lastName("Biswas")
                .lastLoggedIn(new Date())
                .build();

        userRepository.deleteAll();
        userRepository.save(initialUser);
    }
}

