package titas.micro.trello.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import titas.micro.trello.entities.Board;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface BoardRepository extends JpaRepository<Board, Long> {
    Optional<Board> findByName(String name);

}