package titas.micro.trello.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import titas.micro.trello.entities.BoardColumn;

import java.util.Optional;

@Repository
public interface BoardColumnRepository extends JpaRepository<BoardColumn, Long>{
    Optional<BoardColumn> findByName(String name);

}