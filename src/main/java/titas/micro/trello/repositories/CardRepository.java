package titas.micro.trello.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import titas.micro.trello.entities.Card;

import java.util.Date;
import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findByLabels_LabelName(String labelName);

    List<Card> findByCreationDateGreaterThanEqual(Date creationDate);

    List<Card> findByCreationDateGreaterThanEqualOrLastModifiedDateGreaterThanEqual(Date creationDate, Date lastModifiedDate);


}