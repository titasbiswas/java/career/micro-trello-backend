package titas.micro.trello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroTrelloBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroTrelloBackendApplication.class, args);
	}

}
